Name:                joni
Version:             2.2.1
Release:             1
Summary:             Java port of Oniguruma regexp library
License:             MIT
URL:                 https://github.com/jruby/%{name}
Source0:             https://github.com/jruby/joni/archive/refs/tags/%{name}-%{version}.tar.gz
BuildArch:           noarch
BuildRequires:       java-devel jcodings jpackage-utils junit maven-local maven-compiler-plugin
BuildRequires:       maven-jar-plugin maven-surefire-plugin sonatype-oss-parent
BuildRequires:       objectweb-asm
Requires:            jcodings jpackage-utils objectweb-asm
%description
joni is a port of Oniguruma, a regular expressions library,
to java. It is used by jruby.

%package help
Summary:             Javadoc for %{name}
Requires:            jpackage-utils
%description help
API documentation for %{name}.

%prep
%autosetup -n %{name}-%{name}-%{version} -p1

find -type f '(' -iname '*.jar' -o -iname '*.class' ')' -print -delete

# Remove pointless parent pom
%pom_remove_parent

# Remove wagon extension
%pom_xpath_remove 'pom:build/pom:extensions'

# Remove plugins not relevant for downstream RPM builds
%pom_remove_plugin :maven-javadoc-plugin
%pom_remove_plugin :maven-source-plugin

%build
%mvn_build -j -- -P !release-on-9

# Avoid module not found: org.objectweb.asm org.jruby.jcodings
find -name module-info.java -delete
xmvn --batch-mode --offline org.fedoraproject.xmvn:xmvn-mojo:javadoc

%install
%mvn_install

%files -f .mfiles
%doc MANIFEST.MF README.md
%license LICENSE

%files help -f .mfiles-javadoc

%changelog
* Mon Dec 16 2024 Ge Wang <wang__ge@126.com> - 2.2.1-1
- Upgrade joni to 2.2.1
 
* Wed Sep 27 2023 Ge Wang <wang__ge@126.com> - 2.1.44-1
- Upgrade joni to 2.1.44

* Tue Feb 15 2022 yaoxin <yaoxin30@huawei.com> - 2.1.24-1
- Upgrade joni to 2.1.24 to resolve compilation failures.

* Mon Jul 27 2020 shaoqiang kang <kangshaoqiang1@huawei.com> - 2.1.3-1
- Package init
